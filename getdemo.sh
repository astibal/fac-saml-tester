#!/bin/sh
VER="2.4.2"
FNM="v${VER}.zip"
wget -c https://github.com/onelogin/python-saml/archive/$FNM
unzip ${FNM}
P=`pwd`


cd "python-saml-${VER}/demo-bottle/"
mv saml/settings.json saml/settings.json.old
cat saml/settings.json.old | tr -d '<>' > saml/settings.json
cp -r * $P
cd $P

#cleanup
rm ${FNM}
rm -r "python-saml-${VER}"

from bottle import get, post, request, redirect, jinja2_view, error, static_file
from bottle import Bottle, run
import json
import re
import sys, time, socket
import os, signal, threading, thread
import pprint

from index import *

OZ = {}
OZ["SP_IP"] = "0.0.0.0"
OZ["SP_PORT"] = 8008


def touch(fname, times=None):
    with open(fname, 'a'):
        os.utime(fname, times)

kill_event = threading.Event()

def killer():
    kill_event.wait() # Wait for the kill signal
    time.sleep(1) # Give the server a chance to answer the last request
    thread.interrupt_main() # terminate the server running in the main thread


kill_thread = threading.Thread(target=killer)
kill_thread.daemon=True
kill_thread.start()


def get_settings():
    settings = {}
    try:
        with open("saml/settings.json") as fd:
            try:
                settings = json.load(fd)
                #FIXME
            except TypeError as e:
                print "error loading saml settings.json: " + str(e)
                
    except IOError as e:
        print "error loading saml settings.json: " + str(e)
        
    return settings


@app.route('/reload',method='GET')
def reloadit():
   return '<html><meta http-equiv="refresh" content="5; URL=/"><body>reloading...</body></html>'


@app.route('/showconfig',method='GET')
def showconfig():
    settings = get_settings()
    if settings:
        pp = pprint.pformat(settings)
        ppp = re.sub('\r?\n','</br>',pp)
        return "<pre>" +  ppp + "</pre>"
    
    else:
        return '<html><meta http-equiv="refresh" content="2; URL=/idpsetup"><body>cannot load config</body></html>'



@app.route('/saveconfig', method='GET')
def do_saveconfig():
        try:
            return static_file('settings.json', root='saml/', mimetype='text/json', download="saml-settings.json")
        except IOError as e:
            return "error opening saml settings.json: " + str(e)


@app.route('/loadconfig', method='GET')
@jinja2_view('loadconfig.html', template_lookup=['oztemplates'])
def loadconfig():
    return {}

@app.route('/loadconfig', method='POST')
def do_loadconfig():
    settings = request.files.get('settings')
    if settings:
        settings = settings.file.read()    
    
    with open("saml/settings.json","wb") as fd:
        try:
            fd.write(settings)
        except IOError as e:
            return "error saving saml settings.json: " + str(e)

    # dead end, we need to let demo reload settings.
    kill_event.set()
    
    return redirect("/reload")    
    

@app.route('/idpsetup', method='GET')
@jinja2_view('idpsetup.html', template_lookup=['oztemplates'])
def idpsetup():
    global OZ

    ozsettings = {}
    ozsettings["errors"] = []

    try:
        with open("saml/ozsettings.json") as fd:
            try:
                ozsettings = json.load(fd)
                #FIXME
            except ValueError as e:
                print "error loading saml ozsettings.json: " + str(e)
                ozsettings["errors"].append("OZ settings not valid JSON")
                
    except IOError as e:
        print "error opening file ozsettings.json: " + str(e)
        
        if OZ["SP_IP"] != "0.0.0.0":
            ozsettings['myfqdn'] = OZ["SP_IP"]
        
        ozsettings['myport'] = OZ["SP_PORT"]
    
    return ozsettings

@app.route('/idpsetup',  method='POST')
def do_idpsetup():
    facfqdn = request.forms.get('facfqdn')
    facrandom = request.forms.get('facrandom')
    faccert = request.files.get('faccert')
    spfqdn = request.forms.get('myfqdn')
    spport = ":" + request.forms.get('myport')
    sphttps = request.forms.get('myhttps')
    
    
    #write ozconfig
    ozsettings = {}
    ozsettings['facfqdn'] = facfqdn
    ozsettings['facrandom'] = facrandom
    # ommit faccert, could be fixed later
    ozsettings['myfqdn'] = spfqdn
    ozsettings['myport'] = spport[1:]
    ozsettings['myhttps'] = sphttps
    
    ozdump = json.dumps(ozsettings,indent=4)
    with open("saml/ozsettings.json","wb") as fd:
        try:
            fd.write(ozdump)
        except IOError as e:
            print "error saving ozsettings.json: " + str(e)
    
    
    
    
    sp_prefix = "http://"
    if sphttps == "https":
        sp_prefix = "https://"
        if spport == ":443":
            spport = ""
            
    elif sphttps == "":
        if spport == ":80":
            spport = ""
        
    
    settings = get_settings()

    if not settings:
        return "original config file not found. Cannot write!"
    
    # entities in cleartext
    settings["sp"]["entityId"] = "http://" + spfqdn + spport + "/metadata/"
    settings["sp"]["assertionConsumerService"]["url"] = sp_prefix + spfqdn + spport + "/acs/"
    settings["sp"]["singleLogoutService"]["url"] = sp_prefix + spfqdn + spport + "/?sls"
    
    # entities in cleartext
    settings["idp"]["entityId"] = "http://" + facfqdn + "/saml-idp/" + facrandom + "/metadata/"
    settings["idp"]["singleSignOnService"]["url"] = "https://" + facfqdn + "/saml-idp/" + facrandom + "/login/"
    settings["idp"]["singleLogoutService"]["url"] = "https://" + facfqdn + "/saml-idp/" + facrandom + "/logout/"
    
    
    cert = None
    if faccert:
        rawcert = faccert.file.read()
        cert = re.sub(r"\r?\n",r"\n",rawcert)
        settings["idp"]["x509cert"] = cert

    setdump = json.dumps(settings,indent=4)
    print setdump


    with open("saml/settings.json","wb") as fd:
        try:
            fd.write(setdump)
        except IOError as e:
            print "error saving saml settings.json: " + str(e)

    # dead end, we need to let demo reload settings.
    kill_event.set()
    
    return redirect("/reload")


@app.error(500)
def error_500(error):
    from onelogin.saml2.errors import OneLogin_Saml2_Error
    
    print "error" + str(error.exception)
    
    if(isinstance(error.exception, OneLogin_Saml2_Error)):
        print "instance of OneLogin_Saml2_Error"
        return """<html><meta http-equiv="refresh" content="5; URL=/idpsetup"><body>Error %s: returning to IDP setup</body></html>""" % (str(error),)
    else:
        return "500 - Internal Server Error"


def lookenv(key):
    global OZ
    try:
        t = os.environ[key]
        OZ[key] = t
        
        return t
    except KeyError:
        pass
    
    return None

if __name__ == "__main__":

    try:
        lookenv("SP_IP")
        lookenv("SP_PORT")
        
        run(SessionMiddleware(app, config=session_opts), host=OZ["SP_IP"], port=int(OZ["SP_PORT"]), debug=True, reloader=False, server='paste')
        
    except (ValueError, TypeError, socket.error) as e:
        print "error: " + str(e)
        touch('.terminate')
    except KeyboardInterrupt:
        print "Ctrl-C: bailing out"
        touch('.terminate')
